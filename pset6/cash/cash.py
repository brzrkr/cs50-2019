import cs50

# Get non-negative amount from user.
amount = 0
while not amount > 0:
    amount = cs50.get_float("Change owed: ")

# Convert dollars to cents to avoid floating point error.
amount *= 100

# Values of quarters, dimes, nickels, pennies in descending order, in cents.
coin_values = (25, 10, 5, 1)

coins = 0
# Iterate over values of types of coin.
for c in coin_values:
    # coint amounts of coins of current type needed.
    coins += amount // c
    # Get remaning amount after dividing by coin type.
    amount %= c

    # If the amount is exhausted, we're done.
    if not amount:
        break

print(int(coins))
