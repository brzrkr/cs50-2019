import cs50

# Ask user for name.
name = cs50.get_string("What is your name?\n")

# Print name.
print(f"hello, {name}")
