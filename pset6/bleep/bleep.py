from cs50 import get_string
from sys import argv


def main():
    # Ensure proper usage.
    if len(argv) != 2:
        print("Usage: python bleep.py dictionary")
        exit(1)

    # Open file and collect banned words.
    with open(argv[1], "r") as file:
        banned = set([word.strip() for word in file])

    clean = get_string("What message would you like to censor?\n")

    # Replace banned words with asterisks.
    censored = " ".join(["*" * len(w) if w.lower() in banned else w for w in clean.split()])

    print(censored)


if __name__ == "__main__":
    main()
