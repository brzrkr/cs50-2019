import cs50

size = 0
# Repeat as long as size is not between limits.
while not 1 <= size <= 8:
    size = cs50.get_int("Height: ")

for i in range(1, size + 1):
    # Compute padding.
    padding = " " * (size - i)
    # Create blocks.
    blocks = "#" * i

    # Compose blocks with padding and fixed spacing.
    print(f"{padding}{blocks}  {blocks}")
