import cs50
from itertools import cycle
from sys import argv


# Ensure properly formed argument.
if len(argv) != 2 or not argv[1].isalpha():
    print("Usage: python vigenere.py k")
    exit(1)

# Convert alphabetic key to list of numeric keys.
keys_list = [ord(c.upper()) - 65 for c in argv[1]]


# Generate keys, keeping track of the infinite cycle.
class Key:
    def __init__(self, keys_list):
        self.keys = cycle(keys_list)

    def next(self):
        return next(self.keys)


def encrypt(plaintext, keygen):
    # Iterate through letters.
    ciphertext = ""
    for char in plaintext:
        # Only encrypt letters.
        if not char.isalpha():
            ciphertext += char
            continue

        # Encrypt character maintaining case.
        key = keygen.next()
        shift = 65 if char.isupper() else 97

        encrypted = (ord(char) - shift + key) % 26 + shift
        encrypted = chr(encrypted)

        ciphertext += encrypted

    return ciphertext


if __name__ == "__main__":
    # Get text to encode from user.
    plaintext = cs50.get_string("plaintext: ")
    keygen = Key(keys_list)

    print(f"ciphertext: {encrypt(plaintext, keygen)}")
