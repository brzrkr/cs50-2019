#include <stdio.h>
#include <stdlib.h>
#include <cs50.h>
#include "bmp.h"

int main(int argc, char *argv[])
{
    // Ensure correct number of args.
    if (argc != 4)
    {
        printf("Usage: ./resize n infile outfile\n");
        return 1;
    }

    // Mark argument values.
    int scale_factor = atoi(argv[1]);
    char *infile_p = argv[2];
    char *outfile_p = argv[3];

    // Open input.
    FILE *input_p = fopen(infile_p, "r");

    // Read headers.
    BITMAPFILEHEADER in_head;
    BITMAPINFOHEADER in_info;
    fread(&in_head, sizeof(BITMAPFILEHEADER), 1, input_p);
    fread(&in_info, sizeof(BITMAPINFOHEADER), 1, input_p);

    // Infer padding for input scanlines.
    int in_padding = (4 - (in_info.biWidth * sizeof(RGBTRIPLE)) % 4) % 4;

    // Modify headers in output.
    BITMAPINFOHEADER out_info = in_info;
    out_info.biWidth *= scale_factor;
    out_info.biHeight = out_info.biHeight * scale_factor;

    // Determine padding for output scanlines.
    int out_padding = (4 - (out_info.biWidth * sizeof(RGBTRIPLE)) % 4) % 4;

    out_info.biSizeImage = ((sizeof(RGBTRIPLE) * out_info.biWidth)
                            + out_padding) * abs(out_info.biHeight);

    BITMAPFILEHEADER out_head = in_head;
    out_head.bfSize = out_info.biSizeImage + sizeof(BITMAPFILEHEADER)
                      + sizeof(BITMAPINFOHEADER);

    // Open output.
    FILE *output_p = fopen(outfile_p, "w");

    // Write headers to output.
    fwrite(&out_head, sizeof(BITMAPFILEHEADER), 1, output_p);
    fwrite(&out_info, sizeof(BITMAPINFOHEADER), 1, output_p);

    // Iterate over input lines.
    for (int i = 0; i < abs(in_info.biHeight); i++)
    {
        // Prepare an array for a line.
        RGBTRIPLE line[out_info.biWidth];

        // Iterate over input line pixels.
        for (int j = 0; j < in_info.biWidth; j++)
        {
            RGBTRIPLE triple;

            // Read triple from input.
            fread(&triple, sizeof(RGBTRIPLE), 1, input_p);

            // Write triple to line array as many times as required.
            for (int k = 0; k < scale_factor; k++)
            {
                line[k + (j * scale_factor)] = triple;
            }
        }

        // Skip padding in input.
        fseek(input_p, in_padding, SEEK_CUR);

        // Add line to output as many times as required.
        for (int j = 0; j < scale_factor; j++)
        {
            fwrite(&line, sizeof(line), 1, output_p);

            // Add padding to output.
            for (int k = 0; k < out_padding; k++)
            {
                fputc(0x00, output_p);
            }
        }
    }

    // Close files.
    fclose(input_p);
    fclose(output_p);
    return 0;
}
