#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    // Ensure correct usage.
    if (argc != 2)
    {
        fprintf(stderr, "Usage: ./recover image\n");
        return 1;
    }

    // Ensure valid file.
    FILE *raw = fopen(argv[1], "r");
    if (raw == NULL)
    {
        fprintf(stderr, "File is absent or invalid.\n");
        return 2;
    }

    // Prepare file output.
    char filename[8];
    int fileno = 0;
    FILE *current_file = NULL;

    // Read blocks of memory.
    int block_size = 512;
    unsigned char *block = malloc(block_size);
    while (fread(block, block_size, 1, raw))
    {
        // Detect start of new jpeg.
        if (block[0] == 0xFF &&
            block[1] == 0xD8 &&
            block[2] == 0xFF &&
            (block[3] & 0xF0) == 0xE0)
        {
            // Open new file, closing current one if applicable.
            if (current_file != NULL)
            {
                fclose(current_file);
            }

            sprintf(filename, "%.3i.jpg", fileno);
            current_file = fopen(filename, "w");
            fileno += 1;
        }

        // If no jpeg is open, don't write anything.
        if (current_file != NULL)
        {
            fwrite(block, block_size, 1, current_file);
        }
    }

    free(block);

    return 0;
}
