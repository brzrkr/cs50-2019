// Implements a dictionary's functionality

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dictionary.h"

// Represents number of children for each node in a trie
#define N 27

// Represents a node in a trie
typedef struct node
{
    bool is_word;
    struct node *children[N];
}
node;

// Represents a trie
node *root;

// Count the words in the dictionary.
unsigned long word_count = 0;

// Check if loaded.
bool loaded = false;

int index(char c);
void free_children(node *n);

// Loads dictionary into memory, returning true if successful else false
bool load(const char *dictionary)
{
    // Initialize trie
    root = malloc(sizeof(node));
    if (root == NULL)
    {
        return false;
    }
    root->is_word = false;
    for (int i = 0; i < N; i++)
    {
        root->children[i] = NULL;
    }

    // Open dictionary
    FILE *file = fopen(dictionary, "r");
    if (file == NULL)
    {
        unload();
        return false;
    }

    // Buffer for a word
    char word[LENGTH + 1];

    // Insert words into trie
    while (fscanf(file, "%s", word) != EOF)
    {
        node *cursor = root;

        // Iterate over chars of word.
        for (int i = 0; strncmp(&word[i], "\0", 1) != 0; i++)
        {
            char c = word[i];
            node *next = cursor->children[index(c)];

            // Make node for letter if it doesn't exist.
            if (next == NULL)
            {
                node *new_node = malloc(sizeof(node));
                if (new_node == NULL)
                {
                    return false;
                }
                memset(new_node, 0, sizeof(node));

                cursor->children[index(c)] = new_node;
                next = new_node;
            }

            cursor = next;
        }

        // Assume we found the end of the word.
        cursor->is_word = true;
        word_count++;
    }

    // Close dictionary
    fclose(file);

    // Indicate success
    loaded = true;
    return true;
}

// Returns number of words in dictionary if loaded else 0 if not yet loaded
unsigned int size(void)
{
    if (loaded)
    {
        return word_count;
    }

    return 0;
}

// Returns true if word is in dictionary else false
bool check(const char *word)
{
    node *cursor = root;
    for (int i = 0; strncmp(&word[i], "\0", 1) != 0; i++)
    {
        char c = word[i];
        node *next = cursor->children[index(c)];

        // Check if the next letter of the chain is present.
        if (next == NULL)
        {
            return false;
        }

        cursor = next;
    }

    if (cursor->is_word)
    {
        return true;
    }

    return false;
}

// Unloads dictionary from memory, returning true if successful else false
bool unload(void)
{
    free_children(root);
    word_count = 0;
    loaded = false;

    return true;
}

// Free all children nodes recursively.
void free_children(node *n)
{
    for (int i = 0; i < N; i++)
    {
        node *current = n->children[i];
        if (current)
        {
            free_children(current);
        }
    }

    free(n);
}

// Return the index reserved for the given letter or apostrophe.
int index(char c)
{
    if (c == 39)
    {
        return 26;
    }

    if (c >= 97)
    {
        return c - 97;
    }

    return c - 65;
}
