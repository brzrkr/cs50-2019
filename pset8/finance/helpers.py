import os
import requests
import urllib.parse

from flask import redirect, render_template, request, session
from functools import wraps


def apology(message, code=400):
    """Render message as an apology to user."""
    def escape(s):
        """
        Escape special characters.

        https://github.com/jacebrowning/memegen#special-characters
        """
        for old, new in [("-", "--"), (" ", "-"), ("_", "__"), ("?", "~q"),
                         ("%", "~p"), ("#", "~h"), ("/", "~s"), ("\"", "''")]:
            s = s.replace(old, new)
        return s
    return render_template("apology.html", top=code, bottom=escape(message)), code


def login_required(f):
    """
    Decorate routes to require login.

    http://flask.pocoo.org/docs/1.0/patterns/viewdecorators/
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get("user_id") is None:
            return redirect("/login")
        return f(*args, **kwargs)
    return decorated_function


def lookup(symbol):
    """Look up quote for symbol."""

    # Contact API
    try:
        api_key = os.environ.get("API_KEY")
        response = requests.get(
            f"https://cloud-sse.iexapis.com/stable/stock/{urllib.parse.quote_plus(symbol)}/quote?token={api_key}"
        )
        response.raise_for_status()
    except requests.RequestException:
        return None

    # Parse response
    try:
        quote = response.json()
        return {
            "name": quote["companyName"],
            "price": float(quote["latestPrice"]),
            "symbol": quote["symbol"]
        }
    except (KeyError, TypeError, ValueError):
        return None


def usd(value):
    """Format value as USD."""
    return f"${value:,.2f}"


def get_user_stock(user_id, db):
    """Return a dict of currently owned stocks and amounts thereof."""

    # Get buy and sell transactions for user.
    bought = db.execute("SELECT symbol, SUM(amount) AS quantity FROM purchases "
                        "WHERE user_id=:user_id GROUP BY symbol",
                        user_id=user_id)
    sold = db.execute("SELECT symbol, SUM(amount) AS quantity FROM sells "
                      "WHERE user_id=:user_id GROUP BY symbol",
                      user_id=user_id)

    # Build a dictionary of quantities per symbol.
    user_stock = {stock["symbol"]: stock["quantity"] for stock in bought}
    for stock in sold:
        sym = stock["symbol"]

        # Compute quantity by bought - sold.
        user_stock[sym] = user_stock.get(sym, 0) - stock["quantity"]

        # Remove key if no shares owned for given symbol.
        if user_stock[sym] < 1:
            del user_stock[sym]

    return user_stock
