import os

from cs50 import SQL
from flask import Flask, flash, jsonify, redirect, render_template, request, session, url_for, flash
from flask_session import Session
from tempfile import mkdtemp
from werkzeug.exceptions import default_exceptions, HTTPException, InternalServerError
from werkzeug.security import check_password_hash, generate_password_hash
from datetime import datetime

from helpers import apology, login_required, lookup, usd, get_user_stock
from itertools import chain
import re

# Configure application
app = Flask(__name__)

# Ensure templates are auto-reloaded
app.config["TEMPLATES_AUTO_RELOAD"] = True

# Ensure responses aren't cached
@app.after_request
def after_request(response):
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    response.headers["Expires"] = 0
    response.headers["Pragma"] = "no-cache"
    return response


# Custom filter
app.jinja_env.filters["usd"] = usd

# Configure session to use filesystem (instead of signed cookies)
app.config["SESSION_FILE_DIR"] = mkdtemp()
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

# Configure CS50 Library to use SQLite database
db = SQL("sqlite:///finance.db")

# Make sure API key is set
if not os.environ.get("API_KEY"):
    raise RuntimeError("API_KEY not set")


@app.route("/")
@login_required
def index():
    """Show portfolio of stocks"""
    # Get stocks currently owned by user.
    user_id = session["user_id"]
    user_stock = get_user_stock(user_id, db)

    # Prepare list of dictionaries to hold data on each symbol.
    stocks = []
    for symbol, shares in user_stock.items():
        data = lookup(symbol)
        if data is None:
            data = {"price": 0}

        stocks.append({
            "symbol": symbol,
            "shares": shares,
            "price": data["price"],
            "total": data["price"] * shares,
        })

    # Add informative row if no stocks are owned.
    if not stocks:
        stocks.append({
            "symbol": "No stocks owned yet",
            "shares": 0,
            "price": 0,
            "total": 0,
        })

    # Get total value of holdings and current balance, and grand total of both.
    balance = db.execute("SELECT cash FROM users WHERE id=:user_id",
                         user_id=user_id)[0]["cash"]

    tot_value = sum([stock["total"] for stock in stocks])
    grand_tot = tot_value + balance

    return render_template("index.html",
                           balance=balance,
                           tot_value=tot_value,
                           grand_tot=grand_tot,
                           stocks=stocks)


@app.route("/buy", methods=["GET", "POST"])
@login_required
def buy():
    """Buy shares of stock"""
    if request.method == "POST":
        symbol = request.form.get("symbol")
        shares = request.form.get("shares")

        # Check presence of all fields.
        if not all(field for field in (symbol, shares)):
            return apology("Symbol and # of shares are required!")

        if not shares.isdecimal():
            return apology("# of shares must be an integer!")

        shares = int(shares)

        # Check that shares is a positive integer.
        if shares < 1:
            return apology("You must select a positive amount of shares!")

        # Check that requested symbol exists.
        quote = lookup(symbol)
        if quote is None:
            return apology("The specified symbol wasn't found.")

        # Check that user has enough funds.
        user_id = session["user_id"]
        user_funds = db.execute("SELECT cash FROM users WHERE id=:user_id",
                                user_id=user_id)[0]["cash"]
        total_cost = quote["price"] * shares
        if user_funds < total_cost:
            return apology("You don't have enough money!")

        # Deduct the funds and effect the transaction.
        new_funds = user_funds - total_cost
        db.execute("UPDATE users SET cash=:new_funds WHERE id=:user_id",
                   new_funds=new_funds, user_id=user_id)
        db.execute("INSERT INTO purchases "
                   "('user_id', 'symbol', 'amount', 'price', 'timestamp') "
                   "VALUES (:user_id, :symbol, :amount, :price, :timestamp)",
                   user_id=session["user_id"], symbol=symbol, amount=shares,
                   price=quote["price"], timestamp=datetime.now())

        flash(f"Successfully bought {shares} of {symbol.upper()} for {usd(total_cost)}.")

        return redirect(url_for("index"))
    else:
        sym = request.args.get("sym")

        return render_template("buy.html", sym=sym)


@app.route("/check", methods=["GET"])
def check():
    """Return true if username available, else false, in JSON format"""
    username = request.args.get("username")

    # Check valid length.
    if len(username) < 1:
        return jsonify(False)

    # Check availability.
    if len(db.execute("SELECT * FROM users WHERE username=:username", username=username)):
        return jsonify(False)

    return jsonify(True)


@app.route("/history")
@login_required
def history():
    """Show history of transactions"""
    user_id = session["user_id"]

    sells = db.execute("SELECT symbol, amount, price, timestamp FROM sells "
                       "WHERE user_id=:user_id", user_id=user_id)
    purchases = db.execute("SELECT symbol, amount, price, timestamp FROM purchases "
                           "WHERE user_id=:user_id", user_id=user_id)

    transactions = []
    for transaction in chain(sells, purchases):
        transactions.append({
            "symbol": transaction["symbol"],
            "amount": transaction["amount"],
            "price": transaction["price"],
            "total": transaction["price"] * transaction["amount"],
            "timestamp": transaction["timestamp"],
            "is_sell": True if transaction in sells else False,
        })

    # Add informative row if no transactions yet.
    if not transactions:
        transactions.append({
            "symbol": "No transactions to show yet.",
            "amount": 0,
            "price": 0,
            "total": 0,
            "timestamp": 0,
            "is_sell": False,
        })

    return render_template("history.html", transactions=transactions)


@app.route("/login", methods=["GET", "POST"])
def login():
    """Log user in"""

    # Forget any user_id
    session.clear()

    # User reached route via POST (as by submitting a form via POST)
    if request.method == "POST":

        # Ensure username was submitted
        if not request.form.get("username"):
            return apology("must provide username", 403)

        # Ensure password was submitted
        elif not request.form.get("password"):
            return apology("must provide password", 403)

        # Query database for username
        rows = db.execute("SELECT * FROM users WHERE username = :username",
                          username=request.form.get("username"))

        # Ensure username exists and password is correct
        if len(rows) != 1 or not check_password_hash(rows[0]["hash"], request.form.get("password")):
            return apology("invalid username and/or password", 403)

        # Remember which user has logged in
        session["user_id"] = rows[0]["id"]

        # Message success.
        flash("You are now logged in.")

        # Redirect user to home page
        return redirect("/")

    # User reached route via GET (as by clicking a link or via redirect)
    else:
        return render_template("login.html")


@app.route("/logout")
def logout():
    """Log user out"""

    # Forget any user_id
    session.clear()

    # Redirect user to login form
    return redirect("/")


@app.route("/quote", methods=["GET", "POST"])
@login_required
def quote():
    """Get stock quote."""
    if request.method == "POST":
        symbol = request.form.get("symbol")

        if not symbol:
            return apology("A symbol is required.")

        if not re.match(r"[a-zA-Z]{1,5}", symbol):
            return apology("A symbol can only contain uppercase letters and be 1 to 5 characters long.")

        quoted = lookup(symbol)
        if not quoted:
            return apology("Symbol not found.")

        return render_template("quoted.html", quoted=quoted)
    else:
        return render_template("quote.html")


@app.route("/register", methods=["GET", "POST"])
def register():
    """Register user"""

    if request.method == "POST":
        # Check if username exists or is blank.
        username = request.form.get("username")
        if not username:
            return apology("A username is required!")

        if db.execute("SELECT * FROM users WHERE username=:username", username=username):
            return apology("This username is already taken!")

        # Check if password and confirmation are valid.
        password = request.form.get("password")
        confirm = request.form.get("confirmation")
        if not all(field for field in (password, confirm)) or password != confirm:
            return apology("Password and confirmation must match and not be blank!")

        # Register the user.
        db.execute("INSERT INTO users "
                   "('username', 'hash') "
                   "VALUES (:username, :hashed)",
                   username=username, hashed=generate_password_hash(password))

        # Message success.
        flash("Registration successful. You can now log in.")

        # Additional feature: immediately log user in.
        # Or so I wanted to do but check50 is confused by it. :(
        return render_template("login.html")
    else:
        return render_template("register.html")


@app.route("/sell", methods=["GET", "POST"])
@login_required
def sell():
    """Sell shares of stock"""
    # Get stocks currently owned by user.
    user_id = session["user_id"]
    user_stock = get_user_stock(user_id, db)

    if request.method == "POST":
        symbol = request.form.get("symbol").upper()
        shares = request.form.get("shares")

        if not all(field for field in (symbol, shares)):
            return apology("All fields are required!")

        if not shares.isdecimal():
            return apology("# of shares must be an integer!")

        shares = int(shares)

        if shares < 1:
            return apology("You must select a positive amount of shares!")

        if shares > user_stock[symbol]:
            return apology(f"You don't own {shares} shares of {symbol} to sell!")

        # Add the funds and effect the transaction.
        user_funds = db.execute("SELECT cash FROM users WHERE id=:user_id",
                                user_id=user_id)[0]["cash"]
        price = lookup(symbol)["price"]
        total_gains = price * shares
        new_funds = user_funds + total_gains

        db.execute("UPDATE users SET cash=:new_funds WHERE id=:user_id",
                   new_funds=new_funds, user_id=user_id)
        db.execute("INSERT INTO sells "
                   "('user_id', 'symbol', 'amount', 'price', 'timestamp') "
                   "VALUES (:user_id, :symbol, :amount, :price, :timestamp)",
                   user_id=user_id, symbol=symbol, amount=shares,
                   price=price, timestamp=datetime.now())

        # Refresh user stock before redirecting.
        user_stock = get_user_stock(user_id, db)

        flash(f"Successfully sold {shares} of {symbol.upper()}.")

        return redirect(url_for("index"))
    else:
        sym = request.args.get("sym")

        return render_template("sell.html", owned_stocks=list(user_stock.keys()), sym=sym)


@app.route("/top-up", methods=["GET", "POST"])
@login_required
def top_up():
    """Top-up user account."""
    # Get current data for user.
    user_id = session["user_id"]
    user_data = db.execute("SELECT cash FROM users WHERE id=:user_id",
                           user_id=user_id)[0]
    current_balance = user_data["cash"]

    if request.method == "POST":
        amount = float(request.form.get("amount") or 0)

        # Check that amount is a positive number.
        if amount < 1:
            return apology("You must select a positive amount greater than $1.00!")

        # Increase user funds. For free. :D
        db.execute("UPDATE users SET cash=:new_funds WHERE id=:user_id",
                   new_funds=current_balance + amount, user_id=user_id)

        flash(f"Successfully added {amount} to your account!")

        return redirect(url_for("index"))
    else:
        return render_template("top_up.html", current_balance=current_balance)


def errorhandler(e):
    """Handle error"""
    if not isinstance(e, HTTPException):
        e = InternalServerError()
    return apology(e.name, e.code)


# Listen for errors
for code in default_exceptions:
    app.errorhandler(code)(errorhandler)
