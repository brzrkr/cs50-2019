from nltk import sent_tokenize


def lines(a, b):
    """Return lines in both a and b"""
    lines = [{line.strip() for line in string.split("\n")} for string in (a, b)]

    return list(lines[0].intersection(lines[1]))


def sentences(a, b):
    """Return sentences in both a and b"""
    sentences = [{sentence.strip() for sentence in sent_tokenize(string)} for string in (a, b)]

    return list(sentences[0].intersection(sentences[1]))


def substrings(a, b, n):
    """Return substrings of length n in both a and b"""

    substrings = {a: set(), b: set()}
    for string in (a, b):
        for i in range(len(string) - n + 1):
            sub = string[i:i + n].strip()
            if len(sub) == n:
                substrings[string].add(sub)

    return list(substrings[a].intersection(substrings[b]))
