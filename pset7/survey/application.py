import cs50
import csv

from flask import Flask, jsonify, redirect, render_template, request, url_for

# Configure application
app = Flask(__name__)

# Reload templates when they are changed
app.config["TEMPLATES_AUTO_RELOAD"] = True

# Global field names for use in /form and /sheets.
fields = ["name", "breed", "gender"]


@app.after_request
def after_request(response):
    """Disable caching"""
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    response.headers["Expires"] = 0
    response.headers["Pragma"] = "no-cache"
    return response


@app.route("/", methods=["GET"])
def get_index():
    return redirect("/form")


@app.route("/form", methods=["GET"])
def get_form():
    return render_template("form.html")


@app.route("/form", methods=["POST"])
def post_form():
    # Check that all fields have been filled.
    if not all([f_value for f_name in fields for f_value in request.form.get(f_name)]):
        return render_template("error.html", message="<p>All fields are required!</p>")

    # Write values to csv.
    with open("survey.csv", "a", newline="") as db:
        writer = csv.DictWriter(db, fieldnames=fields, extrasaction="ignore")
        writer.writerow(request.form)

    return redirect(url_for("get_sheet"))


@app.route("/sheet", methods=["GET"])
def get_sheet():
    # Read csv file.
    with open("survey.csv", "r", newline="") as db:
        reader = csv.DictReader(db, fieldnames=fields)
        cats = list(reader)

    # Pass data to template as iterable of dictionaries.
    return render_template("sheets.html", cats=cats)
